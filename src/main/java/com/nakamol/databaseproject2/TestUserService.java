/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nakamol.databaseproject2;

import com.nakamol.databaseproject2.model.User;
import com.nakamol.databaseproject2.service.UserService;

/**
 *
 * @author werapan
 */
public class TestUserService {

    public static void main(String[] args) {
        UserService userSevice = new UserService();
        User user = userSevice.login("user2", "password");
        if (user != null) {
            System.out.println("Welcome user : " + user.getName());
        } else {
            System.out.println("Error");
        }

    }
}
