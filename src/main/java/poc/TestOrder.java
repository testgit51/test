/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poc;

import com.nakamol.databaseproject.dao.OrdersDao;
import com.nakamol.databaseproject.model.OrderDetail;
import com.nakamol.databaseproject.model.Orders;
import com.nakamol.databaseproject.model.Product;

/**
 *
 * @author OS
 */
public class TestOrder {

    public static void main(String[] args) {
        Product product1 = new Product(1, "A", 75);
        Product product2 = new Product(2, "B", 180);
        Product product3 = new Product(3, "C", 70);
        Orders order = new Orders();
//    OrderDetail orderDetail1 = new OrderDetail(product1, product1.getName(), product1.getPrice(), 1, order);

        order.addOrderDetail(product1, 10);
        order.addOrderDetail(product2, 5);
        order.addOrderDetail(product3, 3);

//        System.out.println(order);
//        System.out.println(order.getOrderDetails());
//        printReciept(order);

        OrdersDao orderDao = new OrdersDao();
        Orders newOrders = orderDao.save(order);
        System.out.println(newOrders);
        
        Orders order1 = orderDao.get(6);
        printReciept(order1);

    }

    static void printReciept(Orders order) {
        System.out.println("Order" + order.getId());
        for (OrderDetail od : order.getOrderDetail()) {
            System.out.println("" + od.getProductName() + "" + od.getQty() + "" + od.getProductPrice() + "" + od.getTotal());
        }
        System.out.println("Total:" + order.getTotal() + " Qty: " + order.getQty());
    }
}
